'''
This is an example output of wiktionaryparser. Use this dict in UX tests to save time.
'''

etymologies = [
    {
        'etymology': 'From meta-, back-formed from metaphysics.\n',
        'definitions': [
            {
                'partOfSpeech': 'adjective',
                'text': [
                    'meta (comparative more meta, superlative most meta)',
                    '(informal) Self-referential; structured analogously, but at a higher level.'
                ],
                'relatedWords': [],
                'examples': [
                    'Suppose you have a genie that grants you three wishes. If you wish for infinite wishes, that is a meta wish.',
                    '[…]  in finessing obligations you fail a "meta" kind of obligation.',
                    'Besides, I can just hear Vaughan: "Very funny, Stacey, very Charlie Kaufman-esque, very meta, very \'97. I can\'t use it."'
                ]
            }
        ],
        'pronunciations': {
            'text': [
                '(Received Pronunciation) IPA: /ˈmɛtə/',
                '(General American) IPA: /ˈmɛtə/, /mɛɾə/',
                'Rhymes: -ɛtə',
                '(Received Pronunciation) IPA: /ˈmiːtə/',
                '(General American) IPA: /ˈmiːtə/, /miːɾə/',
                'Rhymes: -iːtə',
                '(Received Pronunciation) IPA: /ˈmɛtə/',
                '(General American) IPA: /ˈmɛtə/, /mɛɾə/',
                'Rhymes: -ɛtə',
                '(Received Pronunciation) IPA: /ˈmɛtə/',
                '(General American) IPA: /ˈmɛtə/',
                'Rhymes: -ɛtə'
            ],
            'audio': []
        }
    },
    {
        'etymology': 'From Latin mēta.\n',
        'definitions': [
            {
                'partOfSpeech': 'noun',
                'text': [
                    'meta (plural metas)',
                    'Boundary marker.',
                    'Either of the conical columns at each end of a Roman circus.'
                ],
                'relatedWords': [],
                'examples': []
            }
        ],
        'pronunciations': {
            'text': [
                '(Received Pronunciation) IPA: /ˈmɛtə/',
                '(General American) IPA: /ˈmɛtə/, /mɛɾə/',
                'Rhymes: -ɛtə',
                '(Received Pronunciation) IPA: /ˈmiːtə/',
                '(General American) IPA: /ˈmiːtə/, /miːɾə/',
                'Rhymes: -iːtə',
                '(Received Pronunciation) IPA: /ˈmɛtə/',
                '(General American) IPA: /ˈmɛtə/, /mɛɾə/',
                'Rhymes: -ɛtə',
                '(Received Pronunciation) IPA: /ˈmɛtə/',
                '(General American) IPA: /ˈmɛtə/',
                'Rhymes: -ɛtə'
            ],
            'audio': []
        }
    },
    {
        'etymology': 'Clipping of metagame.\n',
        'definitions': [
            {
                'partOfSpeech': 'noun',
                'text': [
                    'meta (plural metas)',
                    '(video games) Metagame; the most effective tactics and strategies used in a competitive video game.'
                ],
                'relatedWords': [],
                'examples': []
            },
            {
                'partOfSpeech': 'adjective',
                'text': [
                    'meta (comparative more meta, superlative most meta)',
                    '(video games) Prominent in the metagame; effective and frequently used in competitive gameplay.'
                ],
                'relatedWords': [],
                'examples': [
                    "I don't think the character will be meta even with the recent buffs."
                ]
            }
        ],
        'pronunciations': {
            'text': [
                '(Received Pronunciation) IPA: /ˈmɛtə/',
                '(General American) IPA: /ˈmɛtə/, /mɛɾə/',
                'Rhymes: -ɛtə',
                '(Received Pronunciation) IPA: /ˈmiːtə/',
                '(General American) IPA: /ˈmiːtə/, /miːɾə/',
                'Rhymes: -iːtə',
                '(Received Pronunciation) IPA: /ˈmɛtə/',
                '(General American) IPA: /ˈmɛtə/, /mɛɾə/',
                'Rhymes: -ɛtə',
                '(Received Pronunciation) IPA: /ˈmɛtə/',
                '(General American) IPA: /ˈmɛtə/', 'Rhymes: -ɛtə'
            ],
            'audio': []
        }
    },
    {
        'etymology': 'Clipping of metaoidioplasty.\n',
        'definitions': [
            {
                'partOfSpeech': 'noun',
                'text': [
                    'meta (plural metas)',
                    '(informal) Metoidioplasty.'
                ],
                'relatedWords': [],
                'examples': []
            }
        ],
        'pronunciations': {
            'text': [
                '(Received Pronunciation) IPA: /ˈmɛtə/',
                '(General American) IPA: /ˈmɛtə/, /mɛɾə/',
                'Rhymes: -ɛtə',
                '(Received Pronunciation) IPA: /ˈmiːtə/',
                '(General American) IPA: /ˈmiːtə/, /miːɾə/',
                'Rhymes: -iːtə',
                '(Received Pronunciation) IPA: /ˈmɛtə/',
                '(General American) IPA: /ˈmɛtə/, /mɛɾə/',
                'Rhymes: -ɛtə',
                '(Received Pronunciation) IPA: /ˈmɛtə/',
                '(General American) IPA: /ˈmɛtə/',
                'Rhymes: -ɛtə'
            ],
            'audio': []
        }
    }
]
